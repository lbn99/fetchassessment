# Fetch Assessment
Hello! This is Lyndon Nguyen's repository for the Fetch coding assessment. 

# Instructions to run
To run the back-end Flask server, navigate to the folder containing app.py. From there, input the command
"flask run" to start up the server. If not already installed, all you need to run is "pip install Flask"
(or maybe pip3 for the Python 3 version) to get Flask. More detailed instructions here: 
https://flask.palletsprojects.com/en/2.2.x/installation/

# How to use
The following API's are available:

[localhost]/add: adds a transaction for a user (assumes only one user)

    - Only available as a POST method

    - Give the transaction in JSON format as part of the body of the request

    - Example transaction format: {'payer': 'DANNON', 'points': 300, 'timestamp': '2020-10-31T10:00:00Z'}

[localhost]/spend: spends the given amount of points for the user

    - Only available as a POST method

    - Give the points in JSON format as part of the body of the request

    - Example points format: {'points': 100}

[localhost]/points: returns the total points per payer currently for the user

    - Only available as a GET method
