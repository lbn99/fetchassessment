from datetime import datetime
from flask import Flask, request
import os
import ast
from transaction import Transaction

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    transaction_file_dir = "./transactions.txt"

    @app.route("/add", methods = ["POST"])
    def add_transaction():
        '''
        API route to add a transaction to memory. More generally, this adds a
        transaction for a user.
        '''
        with open(transaction_file_dir, 'a') as f:
            trans_dict = request.json
            trans_dict["pts_left"] = trans_dict["points"]
            f.write(str(trans_dict) + "\n")
        return "Transaction added!"

    @app.route("/spend", methods = ["POST"])
    def spend_points():
        '''
        API route to spend the given amount of points. Points are spent by
        transactional time order. After spending points, this will also update
        the transactions in memory to remove the amount of points spent from
        each relevant transaction.
        '''
        points_to_spend = request.json["points"]
        transaction_list = sorted(get_transactions(), key=lambda t: t.get_timestamp())

        idx = 0
        pts_map = {}
        while points_to_spend > 0:
            transaction = transaction_list[idx]

            if transaction.get_points_left() != 0:
                new_pts_to_spend = transaction.spend_points(points_to_spend)
                if transaction.get_payer() not in pts_map:
                    pts_map[transaction.get_payer()] = 0
                pts_map[transaction.get_payer()] -= points_to_spend - new_pts_to_spend

                points_to_spend = new_pts_to_spend
            idx += 1
        
        # write new totals to the file
        with open(transaction_file_dir, 'w') as f:
            for transaction in transaction_list:
                f.write(str(transaction) + "\n")
        
        return pts_map

    @app.route("/points")
    def point_totals():
        '''
        API route to get the total points per payer and return them as a dictionary
        '''
        transaction_list = get_transactions()
        totals = {}

        for transaction in transaction_list:
            if transaction.get_payer() not in totals:
                totals[transaction.get_payer()] = transaction.get_points_left()
            else:
                totals[transaction.get_payer()] += transaction.get_points_left()
        
        return totals
    
    def get_transactions():
        '''
        Get all current transactions stored in memory and return them in a list
        '''
        transaction_list = []
        with open(transaction_file_dir) as f:
            while True:
                line = f.readline()
                if not line:
                    break

                trans_dict = ast.literal_eval(line)
                datetime_obj = datetime.strptime(trans_dict["timestamp"], '%Y-%m-%dT%H:%M:%SZ')
                transaction_list.append(Transaction(trans_dict, datetime_obj))
                
        return transaction_list
        

    return app