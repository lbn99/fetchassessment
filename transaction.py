from datetime import datetime

'''
Class representing a transaction line stored in memory
'''
class Transaction:
    def __init__(self, trans_dict, dt_obj) -> None:
        self.__payer = trans_dict["payer"]          # Name of the payer
        self.__points = trans_dict["points"]        # Initial points in this transaction
        self.__pts_left = trans_dict["pts_left"]    # Remaining total of points in this transaction (after spending)
        self.__timestamp = dt_obj                   # Timestamp of the transaction
    
    def get_payer(self):
        return self.__payer

    def get_points(self):
        return self.__points

    def get_timestamp(self):
        return self.__timestamp
    
    def spend_points(self, points):
        '''
        Spend points from this transaction. Pass in the total number of points trying to be
        spent. If there are enough points in this transaction and is less than the passed
        in points, use all the points in this transaction. Otherwise, use only the passed
        in number of points from this transaction.
        '''
        if points > self.__pts_left:
            pts_left_to_spend = points - self.__pts_left
            self.__pts_left = 0
            return pts_left_to_spend
        else:
            self.__pts_left -= points
            return 0
    
    def get_points_left(self):
        return self.__pts_left
    
    def __str__(self) -> str:
        '''
        Defines how to give a string representation of this transaction
        '''
        json = {}
        json["payer"] = self.__payer
        json["points"] = self.__points
        json["pts_left"] = self.__pts_left
        json["timestamp"] = self.__timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')

        return str(json)